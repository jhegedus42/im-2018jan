package app._experiments.autowire

import autowire.{Core, Macros}

import scala.concurrent.Future

/**
  * Created by joco on 09/01/2018.
  */
class AWTest {

}
trait Client[PickleType, Reader[_], Writer[_]] {
  type Request = Core.Request[PickleType]
  def read[Result: Reader](p: PickleType): Result
  def write[Result: Writer](r: Result): PickleType

  /**
    * A method for you to override, that actually performs the heavy
    * lifting to transmit the marshalled function call from the [[Client]]
    * all the way to the [[Core.Router]]
    */
  def doCall(req: Request): Future[PickleType]
}


trait Server[PickleType, Reader[_], Writer[_]] {
  type Request = Core.Request[PickleType]
  type Router = Core.Router[PickleType]

  def read[Result: Reader](p: PickleType): Result
  def write[Result: Writer](r: Result): PickleType

  /**
    * A macro that generates a `Router` PartialFunction which will dispatch incoming
    * [[Requests]] to the relevant method on [[Trait]]
    */
//  def route[Trait](target: Trait): Router = macro Macros.routeMacro[Trait, PickleType]
}