package app.server.RESTService.rpc

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.util.ByteString
import app.shared.CommonStrings
import app.shared.comm.autowire.AW_Api_Interface
import app.shared.data.model.User
import app.shared.data.ref.Ref
import app.shared.data.views.UserLineListView
import autowire.Core.Request
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.{Decoder, Encoder, Error, Json}
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._

import scala.concurrent.Future

trait JsonSerializers extends autowire.Serializers[Json, Decoder, Encoder] {
  //TODO error handling improvements (left cases)
  override def write[AnyClassToWrite: Encoder](obj: AnyClassToWrite ): Json = obj.asJson

  override def read[AnyClassToRead](json: Json )(implicit ev: Decoder[AnyClassToRead] ): AnyClassToRead = {
    val either = ev.decodeJson( json )
    if (either.isLeft) throw new Exception( either.left.get )
    either.right.get
  }
}


class AWApi_Impl extends AW_Api_Interface {
  override def getHello(x:String): String = {
    println("aw getHello is called")
    "Hello 42"+x
  }

  override def getUserLineListView(u: Ref[User]): UserLineListView = ???
}

object AutoWireSerializer_circe extends autowire.Server[Json, Decoder, Encoder] with JsonSerializers

object AutoWire_Helper {

  def response(request:ByteString, path:List[String], api:AW_Api_Interface) : Future[String]= {
    val rb2: String = request.utf8String
    val jsons: Either[Error, Map[String, Json]] = decode[Map[String, Json]](rb2)
    val req: Request[Json] = autowire.Core.Request(path, jsons.right.get)
    import scala.concurrent.ExecutionContext.Implicits.global
    val res2: Future[String] = (AutoWireSerializer_circe.route[AW_Api_Interface](api)(req).map(_.noSpaces))
    res2
  }

}

object AutoWire {

  val api=new AWApi_Impl()

  val im_autowire_rpc_api_circe: Route = {

    path( CommonStrings.aw_path / Segments ) {
      s =>
        //todo put this part into an actor
        entity(as[ByteString]){
          rb1=>

            complete {
              AutoWire_Helper.response(rb1,s,api)
            }
        }
    }
  }
}
