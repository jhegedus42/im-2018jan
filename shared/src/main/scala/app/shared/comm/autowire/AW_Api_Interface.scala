package app.shared.comm.autowire

import app.shared.data.model.User
import app.shared.data.ref.Ref
import app.shared.data.views.{UserLineListView, View}
import autowire.Core.Request

import scala.reflect
import scala.reflect.api

/**
  * Created by joco on 24/01/2018.
  */

trait AW_Api_Interface {

  def getHello(x: String ): String

  def getUserLineListView(u: Ref[User] ): UserLineListView

}

trait ReadViewRequests {

  def api: AW_Api_Interface

}

object ViewRequests {
  trait ReadViewRequest[V <: View]

//  case class ReadViewRequestResult[V <: View](params: Request, view: V )

  trait ReadViewRequestOps[V <: View] {
    type Request <: ReadViewRequest[V]
//    type Result = ReadViewRequestResult[V,Request]

//    def execute(p: Request )(implicit api: AW_Api_Interface ): ReadViewRequestResult[V,Request]
  }

  object ReadViewRequestOps {
    type Aux[V <: View, Req] = ReadViewRequestOps[V] { type Request = Req}

    implicit object UserLineListReadViewRequestOpsInstance extends ReadViewRequestOps[UserLineListView] {

      case class UserLineListReadViewRequest(u: Ref[User] ) extends ReadViewRequest[UserLineListView]

      override type Request = UserLineListReadViewRequest

//      override def execute(p: Request )(implicit api: AW_Api_Interface ): Result = ???
//        ReadViewRequestResult( p, api.getUserLineListView( p.u ) )


    }
  }

}
