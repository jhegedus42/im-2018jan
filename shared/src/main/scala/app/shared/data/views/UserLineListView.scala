package app.shared.data.views

import app.shared.data.model.UserLineList
import app.shared.data.ref.RefVal

/**
  * Created by joco on 08/01/2018.
  */
case class UserLineListView(lists:List[RefVal[UserLineList]]) extends View{

}
