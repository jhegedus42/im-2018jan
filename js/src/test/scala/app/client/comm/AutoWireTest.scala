package app.client.comm

import app.client.comm.rest.commands.BeforeTester
import app.shared.comm.autowire.AW_Api_Interface
import org.scalatest.{AsyncFunSuite, Matchers}

import scala.concurrent.{ExecutionContextExecutor, Future}

/**
  * Created by joco on 24/01/2018.
  */
import autowire._

object AutoWireTest {}

class AutowireTest extends AsyncFunSuite with BeforeTester with Matchers {
  implicit override def executionContext: ExecutionContextExecutor =
    scala.scalajs.concurrent.JSExecutionContext.Implicits.queue // this is needed !!!
  val testHello: Unit = test("tesst1") {
    val r: Future[String] = ImAutowireClient_circe[AW_Api_Interface].getHello( "joco" ).call()
    r.map( x => x shouldBe "Hello 42joco" )
  }
  testHello


}
