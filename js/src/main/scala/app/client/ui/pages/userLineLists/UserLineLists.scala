package app.client.ui.pages.userLineLists

import app.client.cache.entityCache.{EntityCacheMap, EntityCacheVal}
import app.client.cache.wrapper.CacheRoot
import app.client.comm.rest.commands.forTesting.Helpers
import app.client.ui.pages.Types.Wrapped_CompConstr
import app.client.ui.pages.lineList.LineList_ReactComp
import app.client.ui.pages.main.root_children.materialUI_children.Pages.Page
import app.client.ui.pages.{LineListCompType, Props2Vanilla, Props2Wrapped, UserLineListsCompType}
import app.shared.data.model.User
import app.shared.data.ref.Ref
import japgolly.scalajs.react.ReactElement
import japgolly.scalajs.react.extra.router.RouterCtl
import japgolly.scalajs.react.{BackendScope, ReactComponentB}

object UserLineListsComp {
  import app.client.ui.pages.Types._

  type Prop = Ref[User]

  type Props = Props2Vanilla[Prop, UserLineListsCompType.type]

  class Backend($ : BackendScope[Props, Unit] ) {

    import japgolly.scalajs.react.vdom.prefix_<^._
    import japgolly.scalajs.react.{Callback, ReactElement, ReactNode}

    def render(props: Props ): ReactElement = {
      val c: EntityCacheMap       = props.cacheMaps.entityCacheMap
      val u: EntityCacheVal[User] = props.cacheMaps.entityCacheMap.getEntity(props.ps)
      <.div(
        "cache:",
        <.br,
        pprint.apply( c.map ).plainText,
        <.br,
        "user uuid:",
        props.ps.uuid.toString,
        <.br,
        "user name:", {
          u.getValue.map(_.v.name)
        },
        <.br,
        "list of linelists that user has:",
        props.cacheMaps.viewCacheMap.getView(),
        <.button( "reset server - needs a page reload after clicking this button", ^.onClick --> Callback {
          Helpers.resetServerToLabelThree()
        } )

        //,
        //        if(!u.isReady()) "UserLineList is loading"
        //        else {
        //          val r: Ref[User] = u.getValue.get.refVal.r
        //          c.getEntity(r)
        //        }
      )
    }
  }

  val compConstr: Vanilla_CompConstr[UserLineListsCompType.type, Prop] =
    ReactComponentB[Props](
      "wrapped " +
        "page component"
    ).backend[Backend]( new Backend( _ ) ).renderBackend.build

}

object UserLineListsWrapping {

  val wrapperHolder: CacheRoot = new CacheRoot()

  val wrapped_CC: Wrapped_CompConstr[UserLineListsCompType.type, UserLineListsComp.Prop] =
    wrapperHolder.wrapper.wrapRootPage[UserLineListsCompType.type, UserLineListsComp.Prop](
      UserLineListsComp.compConstr
    )

}
