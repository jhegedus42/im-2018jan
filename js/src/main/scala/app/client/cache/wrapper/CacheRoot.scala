package app.client.cache.wrapper

import app.client.cache.combinedCache.{ViewAndEntityCache, ViewAndEntityCacheMaps}
import app.client.cache.wrapper.reqHandlers.entity.{EntityReadReqHandler, UpdateEntityReqHandler}
import app.shared.data.model.Entity.{Data, Entity}
import app.shared.data.ref.{Ref, RefVal}
import io.circe.{Decoder, Encoder}

import scala.reflect.ClassTag
//todolater make this untyped ... RefDyn

//case class ReadViewRequestInReactComp[V <: Data: ClassTag](ref: ()=>V )
case class ReadEntityRequest[E <: Data: ClassTag](ref: Ref[E] )

case class UpdateEntityRequest[E <: Data](rv: RefVal[E] )

trait StateSettable {
  def setState(c: ViewAndEntityCacheMaps)

}

class CacheRoot() {

////  def getNewCacheMap=new EntityCacheMap(cacheRoot =this) //mutable - coz its a var
//  def getNewCacheMap:CacheMap

//  private[this] val cache: Cache = new EntityCache( this ) //mutable state
  val cache: ViewAndEntityCache = ???
  // egyszerre csak 1 updateRequest futhat (fut=Future el van kuldve)

  private[wrapper] var currently_routed_page: Option[StateSettable] = None

  lazy val wrapper = new ReactCompWrapper( re = this, cm = cache )

  private[wrapper] val entityReadReqHandler = new EntityReadReqHandler(cache.entityCache, reRenderCurrentlyRoutedPageComp _)

//  def clearCache = {
//    cache.resetCache()
//    reRenderCurrentlyRoutedPageComp()
//  }

  private[cache] def handleReadRequest[E <: Entity](rr: ReadEntityRequest[E] ): Unit = entityReadReqHandler.queRequest(rr)

  private[this] def reRenderCurrentlyRoutedPageComp(): Unit = {
    val c: ViewAndEntityCacheMaps = cache.getCacheMap
    println( "re render with cache: " + c )
    currently_routed_page.foreach( {
      s =>
        s.setState( c )
    } )
  }

  private[cache] def handleEntityUpdateReq[E<:Entity:ClassTag : Decoder: Encoder](er:UpdateEntityRequest[E]) : Unit =  // mutates cache, rerenders page
    UpdateEntityReqHandler.launchUpdateReq(cache.entityCache, er, reRenderCurrentlyRoutedPageComp _)
}


