package app.client.cache.wrapper.reqHandlers.view

import app.client.cache.viewCache.{ViewLoading, ViewNotYetLoaded, ViewCache}
import app.client.cache.wrapper.ReadEntityRequest
import app.client.comm.rest.commands.generalCRUD.GetEntityAJAX
import app.client.comm.rest.commands.generalCRUD.GetEntityAJAX.EntResDyn
import app.shared.SomeError_Trait
import app.shared.data.model.Entity.Entity
import app.shared.data.ref.RefVal
import slogging.LazyLogging

import scala.concurrent.Future
import scalaz.{-\/, \/-}

/**
  * Created by joco on 29/12/2017.
  */
class ViewReadReqHandler(cache: ViewCache, pageRerenderer: () => Unit)
  extends LazyLogging {

    private[this] var readViewRequests: Set[ReadEntityRequest[_ <: Entity]] = Set()
    private[wrapper] def queRequest[E <: Entity](rr: ReadEntityRequest[E]): Unit = {
      readViewRequests = readViewRequests + rr
      logger.trace("in collect read requests - readViewRequests:" + rr)
    }

    private[wrapper] def executeReadRequests(): Unit = {
      logger.trace("in executeReadRequests read requests:" + readViewRequests)
      println("137" + readViewRequests)

      def processRequest[E <: Entity]: (ReadEntityRequest[E]) => Future[Unit] = {
        (rr: ReadEntityRequest[E]) =>
          {
            logger.trace("...")
            println("processing read request " + rr)
            import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

            val notYetLoaded: ViewNotYetLoaded[E] = ???
//              cache.setNotYetLoaded(rr.ref)
            // start Future

            val res: Future[EntResDyn] = ??? // GetEntityAJAX.getEntityDyn(rr.ref)

            val loading: ViewLoading[E] = cache.setLoading(notYetLoaded)

            res.map({ r: EntResDyn =>
              println(s"future completed for $rr, result is $r")
              val res: Unit = r.erv match {
                case -\/(err) =>
                  cache.setReadFailed[E](loading, err.toString)
                case \/-(refValDyn) =>
                  refValDyn.toRefVal_NoClassTagNeeded(rr.ref.dataType) match {
                    case -\/(a: SomeError_Trait) => ???
//                      cache.setReadFailed(oldVal = loading,
//                                                 errorMsg = a.string)

//                    case \/-(b: RefVal[E]) =>
//                      cache.setLoaded[E](oldVal = loading, newVal = b)
                  }
              }
              pageRerenderer() // we rerender the page after each ajax came back
            })
            //637e3709d2fa4bd19e9167d45b58c425 commit b644e0744804cc562d4c7648aafaae93ec4727e5 Mon Dec 18 00:18:55 EET 2017
          }
      }

      readViewRequests.foreach(
        (x: ReadEntityRequest[_ <: Entity]) => {

          processRequest(x) //we launch an AJAX call for each entity to be read
        }
      )

      readViewRequests = Set()
      pageRerenderer() // we rerender the page after each ajax req has been started
    }

  }
