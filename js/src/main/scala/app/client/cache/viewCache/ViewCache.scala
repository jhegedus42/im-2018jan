package app.client.cache.viewCache

import app.client.cache.entityCache.{UpdateFailed, Updating}
import app.client.cache.wrapper.CacheRoot
import app.shared.comm.autowire.ViewRequests.{ReadViewRequest, ReadViewRequestOps }
import app.shared.data.model.Entity.Entity
import app.shared.data.ref.{Ref, RefVal}
import app.shared.data.views.View
import cats.macros.Ops

//mutable state
private[cache] class ViewCache(cacheRoute:CacheRoot) {
  private[this] var wrappedMap:ViewCacheMap = new ViewCacheMap(cacheRoot = cacheRoute)

  def getCacheMap() = wrappedMap

  def resetCache() = wrappedMap = new ViewCacheMap(cacheRoot = cacheRoute)

  def setNotYetLoaded[V <: View](ref: ReadViewRequest[V]): ViewNotYetLoaded[V] = {
    val res = ViewNotYetLoaded(ref)
    wrappedMap = wrappedMap.copy(immutableMap = wrappedMap.immutableMap.updated(ref, res))
    res
  }

  def setLoading[V <:View](e: ViewNotYetLoaded[V]): ViewLoading[V] = {

    val res: ViewLoading[V] = ViewLoading(e.ref)
    updateCache(e.ref, res, e)
    res

  }


  // what do we want here ?
//  def setLoaded[V <:View]
//      (oldVal: ViewLoading[V], newVal:ReadViewRequestResult[V] )
//                                 (implicit readViewRequestOps: ReadViewRequestOps[V]) ={
//
//   // updateCache(newVal, ViewLoaded(newVal), oldVal)
//    ???
//  }


  def updateCache[V <:View](key: ReadViewRequest[V],
                               cacheVal: ViewCacheVal[V],
                               oldVal: ViewCacheVal[V]): Unit = {
    assert(wrappedMap.immutableMap(key).equals(oldVal))
    val newCacheMap: Map[ReadViewRequest[_ <: View], ViewCacheVal[_ <: View]] =
      wrappedMap.immutableMap.updated(key, cacheVal)
    wrappedMap = wrappedMap.copy(immutableMap = newCacheMap)
  }



  def setReadFailed[V <:View](oldVal: ViewLoading[V], errorMsg: String): Unit =
    updateCache(oldVal.ref, ViewReadFailed(oldVal.ref, err = errorMsg), oldVal)

}