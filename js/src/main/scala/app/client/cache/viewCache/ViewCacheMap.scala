package app.client.cache.viewCache

import app.client.cache.wrapper.{CacheRoot, ReadEntityRequest, UpdateEntityRequest}
import app.shared.comm.autowire.ViewRequests.{ReadViewRequest, ReadViewRequestOps}
import app.shared.data.views.View
//import app.client.comm.rest.ClientRestAJAX
import app.shared.data.model.Entity.Entity
import app.shared.data.ref.{Ref, RefVal}
import io.circe.{Decoder, Encoder}
import slogging.LazyLogging

import scala.reflect.ClassTag

case class ViewCacheMap(val immutableMap: Map[ReadViewRequest[_<:View], ViewCacheVal[_<:View]] = Map(),
                        val cacheRoot   : CacheRoot) extends LazyLogging{

//  def getView[V <: View:ClassTag,Request,Result]
//  (r: Request)(implicit readViewRequestOps: ReadViewRequestOps.Aux[V,Request,Result]): ViewCacheVal[V] = {
//    // AUX pattern
//
//    if (!immutableMap.contains(r)) { // csak akkor hivjuk meg ha meg nincs benne a cache-ben ...
////      val rr: ReadViewRequestInReactComp[V] = ReadViewRequestInReactComp(r)
//      val rr: ReadViewRequest[V] = ReadViewRequest(r)
//      logger.trace("getEntity - rr:"+rr)
//      cacheRoot.handleViewReadRequest(rr)
//    }
//
//    val res: ViewCacheVal[_<:View] = immutableMap.getOrElse(r, ViewNotInCache(r))
//
//    res.asInstanceOf[ViewCacheVal[V]]
//  }

}
