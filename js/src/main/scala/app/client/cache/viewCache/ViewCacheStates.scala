package app.client.cache.viewCache

import app.shared.comm.autowire.ViewRequests.ReadViewRequest
import app.shared.data.ref.{Ref, RefVal}
import app.shared.data.views.View

import scala.reflect.ClassTag

sealed abstract class ViewCacheVal[V <: View] {

  def getValue: Option[RefVal[V]] = this match {
    case _: Pending[V] => None
    case _: Failed[V]  => None
    case ViewNotInCache(ref) => None
    case ViewNotYetLoaded(ref) => None
    case  ViewLoaded(refVal) => Some(refVal)
  }
  def isReady(): Boolean = false

  override def toString: String = pprint.apply( this ).plainText

//  def map[G<:View:ClassTag](f: V=>G) : ViewCacheVal[G]

}

abstract class Pending[V <: View] extends ViewCacheVal[V]

abstract class Failed[V <: View] extends ViewCacheVal[V]

abstract class Ready[V <: View]() extends ViewCacheVal[V] {
  def refVal: RefVal[V]
}

case class ViewNotInCache[V <: View](ref: ReadViewRequest[V] ) extends ViewCacheVal[V] {
//  override def map[G<:View:ClassTag](f: (V) => G): NotInCache[G] = new NotInCache[G](ref.changeType[G]())
  // derived from cache ... data type is needed for a monad
}

case class ViewNotYetLoaded[V <: View](ref: ReadViewRequest[V] ) extends ViewCacheVal[V] {
//  override def map[G<:View:ClassTag](f: (V) => G): NotYetLoaded[G] = new NotYetLoaded[G](ref.changeType[G]())
}
// in cache but not yet loaded, not yet tried to load, nothing...
//how can this ever be ?

case class ViewLoading[V <: View](ref: ReadViewRequest[V] ) extends Pending[V] {
//  override def map[G<:View:ClassTag](f: (V) => G): Loading[G] =new Loading[G](ref.changeType[G]())
}

case class ViewLoaded[V <: View](refVal: RefVal[V] ) extends Ready[V] {

  override def isReady(): Boolean = true

//  override def map[G<:View:ClassTag](f: (V) => G): Loaded[G] = new Loaded[G](refVal.map(f))
}

case class ViewReadFailed[V <: View](ref: ReadViewRequest[V], err: String ) extends Failed[V] {
//  override def map[G<:View:ClassTag](f: (V) => G): ReadFailed[G] = new ReadFailed[G](Ref.makeWithUUID[G](ref.uuid),err)
}
