package app.client.cache.combinedCache

import app.client.cache.entityCache.EntityCache
import app.client.cache.viewCache.ViewCache

/**
  * Created by joco on 25/01/2018.
  */
case class ViewAndEntityCache(viewCache:ViewCache, entityCache:EntityCache) {
  def getCacheMap =
    new ViewAndEntityCacheMaps(viewCache.getCacheMap(),
                               entityCache.getCacheMap())


}
