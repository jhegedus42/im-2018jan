package app.client.cache.combinedCache

import app.client.cache.entityCache.EntityCacheMap
import app.client.cache.viewCache.ViewCacheMap

/**
  * Created by joco on 25/01/2018.
  */
case class ViewAndEntityCacheMaps
            (viewCacheMap:ViewCacheMap, entityCacheMap: EntityCacheMap){}
