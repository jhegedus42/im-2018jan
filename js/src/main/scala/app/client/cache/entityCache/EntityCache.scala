package app.client.cache.entityCache

import app.client.cache.combinedCache.{ViewAndEntityCacheMaps, ViewAndEntityCache}
import app.client.cache.wrapper.CacheRoot
import app.shared.data.model.Entity.Entity
import app.shared.data.ref.{Ref, RefVal}

//mutable state
private[cache] class EntityCache(cacheRoute:CacheRoot) {
  private[this] var entityCacheMap = new EntityCacheMap(cacheRoot = cacheRoute)

  def getCacheMap() = entityCacheMap

  def resetCache() = entityCacheMap = new EntityCacheMap(cacheRoot = cacheRoute)

  def setNotYetLoaded[E <: Entity](ref: Ref[E]): NotYetLoaded[E] = {
    val res = NotYetLoaded(ref)
    entityCacheMap = entityCacheMap.copy(map = entityCacheMap.map.updated(ref, res))
    res
  }

  def setLoading[E <: Entity](e: NotYetLoaded[E]): Loading[E] = {

    val res: Loading[E] = Loading(e.ref)
    updateCache(e.ref, res, e)
    res

  }

  def setUpdating[E <: Entity](oldVal: Ready[E],
                             newVal: RefVal[E]): Updating[E] = {
    val updatingTo = Updating(newVal)
    updateCache(oldVal.refVal.r, updatingTo, oldVal)
    updatingTo
  }

  def setLoaded[E <: Entity](oldVal: Loading[E], newVal: RefVal[E]) =
    updateCache(newVal.r, Loaded(newVal), oldVal)

  def setUpdated[E <: Entity](oldVal: Updating[E], newVal: RefVal[E]) =
    updateCache(newVal.r, Updated(newVal), oldVal)

  def updateCache[E <: Entity](key: Ref[E],
                             cacheVal: EntityCacheVal[E],
                             oldVal: EntityCacheVal[E]): Unit = {
    assert(entityCacheMap.map(key).equals(oldVal))
    val newCacheMap: Map[Ref[_ <: Entity], EntityCacheVal[_ <: Entity]] =
      entityCacheMap.map.updated(key, cacheVal)
    entityCacheMap = entityCacheMap.copy(map = newCacheMap)
  }

  def setReadFailed[E <: Entity](oldVal: Loading[E], errorMsg: String): Unit =
    updateCache(oldVal.ref, ReadFailed(oldVal.ref, err = errorMsg), oldVal)

  def setUpdateFailed[E <: Entity](oldVal: Updating[E], errorMsg: String): Unit =
    updateCache(oldVal.refVal.r,
                UpdateFailed(oldVal.refVal, err = errorMsg),
                oldVal)
}