package app.client.cache.entityCache

import app.shared.data.model.Entity.{Data, Entity}
import app.shared.data.ref.{Ref, RefVal}

import scala.reflect.ClassTag

abstract class CacheVal[E<:Data]

sealed abstract class EntityCacheVal[E <: Entity] extends CacheVal[E]{

  //    def getValue : Option[HasValue[E]] = ???
  def getValue: Option[RefVal[E]] = this match {
    case _: Pending[E] => None
    case _: Failed[E]  => None
    case NotInCache( ref )     => None
    case NotYetLoaded( ref )   => None
    case  Loaded( refVal )  => Some( refVal )
    case  Updated( refVal ) => Some( refVal )
  }
  def isReady(): Boolean = false

  override def toString: String = pprint.apply( this ).plainText

  def map[G<:Entity:ClassTag](f: E=>G) : EntityCacheVal[G]

}

abstract class Pending[E <: Entity] extends EntityCacheVal[E]

abstract class Failed[E <: Entity] extends EntityCacheVal[E]

abstract class Ready[E <: Entity]() extends EntityCacheVal[E] {
  def refVal: RefVal[E]
}

case class NotInCache[E <: Entity](val ref: Ref[E] ) extends EntityCacheVal[E] {
  override def map[G<:Entity:ClassTag](f: (E) => G): NotInCache[G] = new NotInCache[G](ref.changeType[G]())
}

case class NotYetLoaded[E <: Entity](val ref: Ref[E] ) extends EntityCacheVal[E] {
  override def map[G<:Entity:ClassTag](f: (E) => G): NotYetLoaded[G] = new NotYetLoaded[G](ref.changeType[G]())
}
// in cache but not yet loaded, not yet tried to load, nothing...
//how can this ever be ?

case class Loading[E <: Entity](val ref: Ref[E] ) extends Pending[E] {
  override def map[G<:Entity:ClassTag](f: (E) => G): Loading[G] =new Loading[G](ref.changeType[G]())
}

case class Updating[E <: Entity](val refVal: RefVal[E] ) extends Pending[E] {
  override def map[G<:Entity:ClassTag](f: (E) => G): Updating[G] = new Updating[G](refVal.map(f))
}
// writing

case class Loaded[E <: Entity](val refVal: RefVal[E] ) extends Ready[E] {

  override def isReady(): Boolean = true

  override def map[G<:Entity:ClassTag](f: (E) => G): Loaded[G] = new Loaded[G](refVal.map(f))
}

case class Updated[E <: Entity](val refVal: RefVal[E] ) extends Ready[E] {
  override def isReady(): Boolean = true

  override def map[G<:Entity:ClassTag](f: (E) => G): Updated[G] = new Updated[G](refVal.map(f))
}

case class UpdateFailed[E <: Entity](val refVal: RefVal[E], err: String ) extends Failed[E] {
  override def map[G<:Entity:ClassTag](f: (E) => G): UpdateFailed[G] = new UpdateFailed[G](refVal.map(f),err)
}

case class ReadFailed[E <: Entity](val ref: Ref[E], err: String ) extends Failed[E] {
  override def map[G<:Entity:ClassTag](f: (E) => G): ReadFailed[G] = new ReadFailed[G](Ref.makeWithUUID[G](ref.uuid),err)
}
