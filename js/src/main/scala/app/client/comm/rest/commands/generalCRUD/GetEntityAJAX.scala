package app.client.comm.rest.commands.generalCRUD

import app.shared.data.model.Entity.Data
import app.shared.data.model.{DataType, LineText, User}
import app.shared.data.ref.{Ref, RefDyn, RefValDyn}
import app.shared.rest.routes_take3.crudCommands.GetEntityCommand
import app.shared.{SomeError_Trait, TypeError}
import io.circe.Decoder
import io.circe.generic.auto._
import io.circe.parser.decode

import scala.concurrent.Future
import scala.reflect.ClassTag
import scalaz.\/

/**
  * Created by joco on 16/12/2017.
  */
object GetEntityAJAX {
  // iden mar ki tudom probalni mindket modszert
  // egy ket ismeros ki tudja probalni, akar 10 is ...
  // sync ... addig nem lehet irni ujbol, amig vissza nem jott az osszes adat, amit lekertunk...
  // ir, elkuld, var valaszokra, ha minden megjott, akkor lehet irni ujbol...
    // kell vmi jelzes, hogy mi mikor irhato... pl mikor lehet atrendezni vmit...
    // reorder list, addig ki kell kapcsolni az uj reorder lehetoseget, amig nem jott vissza valasz arrol, hogy sikerult a reorder...
    // ha a reorder az szarmaztatott view... akkor a view-t azt be kell tolteni... kell vmi update, timing cucc... h processzalod az update-et
    // addig nem lehet uj update-et megengedni amig a regi update-rol nem jott vissza minden info, mert a response/szerver mondja meg
    // hogyan nez ki az uj view..., kliens ezt nem tudja... nem tud denormalizalt view-kat megjeleniteni....
    // nem tudja hogyan kell kiszamolni denormalizalt view-kat, ha nagyon kell, akkor ez vhogy megoldhato...
    // hogy aszinkron modon kapja meg az acknowledgement-et hogy el lett mentve minden...
    // tehat van egy saving ... icon... es ha vegetert a balfaszkodas, es minden entity el lett mentve
    // akkor lehet orulni... ilyenkor az entity-k bol szamolja ki amit megjelenit es o maga kesziti el a
    // denormalizal-t view-t, updet-eli az entit-ket, ujraszamolja a denormalizalt view-t... es ha entitik
    // el lettek mentve, akkor zoldre valtja a lampat, hogy minden saved...
    // de persze ez azt jelenti, hogy tul sok adatot nem tolthet be... bar vszeg tul sok adat nincs is amit valtoztathat...
    // akar az osszes adatot betoltheti magarol...
    // a view-ket szamolo kod, az akar teljes mertek-ben lehet a kliens-en
    // mert 2 fajta adat van : a shared/private...
    // illetve harom fajta : many user read - 1 user write, many ready - many write (ebbol lehetoleg keves legyen), one read - one write
    // szoval az irashoz elvileg kell majd vmi lock, hogy minden up to date, ami a kepernyon van ... az elott nem lehet irni, editalni ...
    // kiirni, h. saving ... es addig blokkolni/befagyasztani a komponenst...

  import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
    def getEntity[E <: Data : ClassTag : Decoder](ref: Ref[E])(implicit gec:GetEntityCommand[E]):
    Future[gec.Result] = {
      val route = gec.queryURL(ref)
//      Ajax
//      .get(route)
//      .map(_.responseText)
//      .map((x: String) => decode[gec.Result](x))
//      .map(x => x.right.get)

      GeneralGetAJAX.get[E](route, gec)(decode[GetEntityCommand[E]#Result])
    }

    // ^^^
    // 65869b2e10f94080b2e1fb350c2be50b
    //              def isRefValsTypeValid[E <: Entity](rv: RefVal[E]): Boolean = {
    //                rv.r.entityType == EntityType.make
    //              }
    //              todolater check returned RefVal's type (RefVal's validity) matches the type of E
    //              use isRefValsTypeValid

    /**
      * Uses getEntity to get an entity using only refDyn, this will be used by calls that are not type-safe,
      * where the type of the entity to be requested is not known at compile time (for example when updating the
      * cache).
      *
      * @param refDyn
      * @return
      */


    def getEntityDyn(refDyn: RefDyn): Future[EntResDyn] = {

      def g[E <: Data : ClassTag](implicit gec:GetEntityCommand[E]): gec.Result => EntResDyn = {

        (result: gec.Result) => EntityCRUDReqResultDyn(result.map(RefValDyn.fromRefValToRefValDyn(_)))
      }

      def getCase[E <: Data : ClassTag]: DataType = {DataType.make[E]}

      def res[E <: Data : ClassTag : Decoder](refDyn: RefDyn)
                                             (implicit gec:GetEntityCommand[E]): Future[EntResDyn] = {

        val refV: \/[TypeError, Ref[E]] = refDyn.toRef[E]()
        val ref: Ref[E] = refV.toEither.right.get

        (getEntity[E](ref)).
        map((x: gec.Result) => g[E](implicitly[ClassTag[E]],implicitly[GetEntityCommand[E]])(x))
      }

      import io.circe.generic.auto._
      refDyn.et match {
        case s if s == getCase[LineText] =>
          res[LineText](refDyn) // todolater abstract this more
        case s if s == getCase[User] =>
          res[User](refDyn)
      }
    }

  case class EntityCRUDReqResultDyn(erv: \/[SomeError_Trait, RefValDyn] )
  type EntResDyn=EntityCRUDReqResultDyn

  }
